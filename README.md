# Desafio Desenvolvedor App

## Ferramentas necessárias para executar o projeto
* Node (v >= 12.18.0)
* Android SDK
* Android Virtual Device ou Celular físico

## Instruções para rodar o projeto
* Executar o comando ~npm install~
* Executar o comando ~npx react-native run-android~ ou ~npx react-native run-android --variant=release~
* Deixei caso necessário um apk do aplicativo já feito caso necessite

## Dificuldades / Problemas encontrados
* Como dito na entrevista, estou sem internet no período que foi realizado o teste, consegui utilizar o pc de um amigo para realizar o teste, esse aplicativo foi o melhor que consegui fazer nas 18h virado
* Gostaria de ter feito algumas animações porem não tinha muito tempo
* O Json é enorme, possui muita informação que sem explicação não entendo a como utilizar, optei por usar um valor padrão para os valores dos produtos porque alguns não possuiam o mesmo.
* Utilizei das melhores práticas, compartimentalizei internamente nas telas os componetes que elas usam, fiz dessa forma apesar de ter alguma repetição de código com o pensamento que esses componentes apesar de terem um visual parecido possuem informações distintas e que podem necessitar de tratamento especial, desta forma fica mais fácil realizar manutenções cirúrgicas, como também permite personalizar caso seja necessário sem quebrar em outros pontos 

Tempo de execução do teste
sáb 05 set 2020 16∶51∶14 -- dom 06 set 2020 10∶35∶51
