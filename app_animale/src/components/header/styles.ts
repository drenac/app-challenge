import styled from 'styled-components/native';
import {Dimensions, StatusBar} from 'react-native';

const {height} = Dimensions.get('window');

export const iconSize = height * 0.03;

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0px 20px;
  margin-top: ${StatusBar.currentHeight ? StatusBar.currentHeight + 10 : 20}px;
`;

export const ContainerIcon = styled.View``;

export const ButtonIcon = styled.TouchableOpacity`
  padding: 6px;
`;

export const Title = styled.Text`
  font-size: ${height * 0.03}px;
  font-family: 'Futura Md BT';
`;
