import React from 'react';

import {Container, ContainerIcon, Title, ButtonIcon, iconSize} from './styles';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

interface Props {
  backButton?: boolean;
  title?: string;
  onPressBack?: () => void;
  onPressAction?: () => void;
  icon?: string;
}

const Header: React.FC<Props> = ({
  backButton,
  title,
  onPressBack,
  icon,
  onPressAction,
}) => {
  return (
    <Container>
      <ContainerIcon>
        {backButton && (
          <ButtonIcon onPress={onPressBack}>
            <Icon name="arrow-left" size={iconSize} />
          </ButtonIcon>
        )}
      </ContainerIcon>
      <Title>{title || 'MENU'}</Title>
      <ContainerIcon>
        <ButtonIcon onPress={onPressAction}>
          {icon == 'close' ? (
            <AntDesign name={'close'} size={iconSize * 1.2} />
          ) : (
            <Icon name={icon || 'handbag'} size={iconSize} />
          )}
        </ButtonIcon>
      </ContainerIcon>
    </Container>
  );
};

export default Header;
