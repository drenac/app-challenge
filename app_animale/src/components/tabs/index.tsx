import React from 'react';

import {Container, Button, Text, iconSize} from './styles';
import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tabs: React.FC<BottomTabBarProps> = ({
  state,
  descriptors,
  navigation,
}) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  function navigateToScreen(name: string) {
    if (name != '') {
      navigation.navigate(name);
    }
  }

  return (
    <Container>
      <Button onPress={() => navigateToScreen('Home')}>
        <SimpleLineIcons name="home" size={iconSize} />
        <Text>início</Text>
      </Button>
      <Button onPress={() => navigateToScreen('Menu')}>
        <SimpleLineIcons name="menu" size={iconSize} />
        <Text>menu</Text>
      </Button>
      <Button onPress={() => navigateToScreen('')}>
        <Ionicons name="bookmark-outline" size={iconSize} />
        <Text>my list</Text>
      </Button>
      <Button onPress={() => navigateToScreen('')}>
        <SimpleLineIcons name="user" size={iconSize} />
        <Text>conta</Text>
      </Button>
    </Container>
  );
};

export default Tabs;
