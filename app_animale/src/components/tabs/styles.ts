import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

interface Props {
  isFocused?: boolean;
}

const {height} = Dimensions.get('window');

export const iconSize = height * 0.025;

export const Container = styled.View`
  flex-direction: row;
  padding: 10px 0;
`;

export const Button = styled.TouchableOpacity`
  flex: 1;
  align-items: center;
`;

export const Text = styled.Text<Props>`
  color: ${(props) => (props.isFocused ? '#673ab7' : '#222')};
`;
