import React from 'react';

import {Container, Title, TextButton, ContainerText} from './styles';

interface Props {
  title: string;
  textButton?: string;
}

const TituloCategoria: React.FC<Props> = ({textButton, title}) => {
  return (
    <Container>
      <Title>{title}</Title>
      {textButton !== '' && (
        <ContainerText>
          <TextButton>{textButton}</TextButton>
        </ContainerText>
      )}
    </Container>
  );
};

export default TituloCategoria;
