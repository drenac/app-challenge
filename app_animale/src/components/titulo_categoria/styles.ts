import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0px 16px;
  margin-top: 32px;
  margin-bottom: 20px;
`;

export const Title = styled.Text`
  font-size: ${height * 0.022}px;
  font-family: 'Futura Md BT';
`;

export const TextButton = styled.Text`
  font-size: ${height * 0.02}px;
`;

export const ContainerText = styled.TouchableOpacity`
  padding: 4px;
`;
