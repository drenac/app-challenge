import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';

interface Props {
  line?: boolean;
}

export const Container = styled.View`
  border-width: ${StyleSheet.hairlineWidth}px;
  border-bottom-width: 0;
  padding: 20px;
  align-items: center;
`;

export const ContainerButton = styled.TouchableOpacity.attrs({
  onPress: () => {},
})<Props>`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 90%;
  padding: 20px 0;
  border-bottom-width: ${(props) =>
    props.line ? StyleSheet.hairlineWidth : 0}px;
`;

export const ContainerIcon = styled.View`
  margin-right: 20px;
`;

export const TextButton = styled.Text`
  flex: 1;
  font-family: 'Futura Bk BT';
`;
