import React from 'react';

import {Container, TextButton, ContainerButton, ContainerIcon} from './styles';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';

const Rodape: React.FC = () => {
  return (
    <Container>
      <ContainerButton line>
        <ContainerIcon>
          <SimpleLineIcons name="location-pin" />
        </ContainerIcon>
        <TextButton>Encontre uma loja</TextButton>
        <SimpleLineIcons name="arrow-right" />
      </ContainerButton>
      <ContainerButton>
        <ContainerIcon>
          <Feather name="message-square" />
        </ContainerIcon>
        <TextButton>Fale com um vendedor</TextButton>
        <SimpleLineIcons name="arrow-right" />
      </ContainerButton>
    </Container>
  );
};

export default Rodape;
