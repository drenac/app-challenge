import {useState} from 'react';

const useProduct = () => {
  const [marked, setMarked] = useState(false);

  return {
    marked,
    setMarked,
  };
};

export default useProduct;
