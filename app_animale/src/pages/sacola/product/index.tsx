import React from 'react';

import {
  Container,
  ContainerIcon,
  ContainerInfo,
  Image,
  Price,
  Selector,
  Title,
  Ref,
  Delete,
  iconSize,
} from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import useProduct from './useProduct';

const sex = [
  {
    name: 'F',
    id: 10,
  },
  {
    name: 'M',
    id: 17,
  },
];

const Product: React.FC = () => {
  const {marked, setMarked} = useProduct();
  return (
    <Container>
      <Image
        source={require('../../../assets/page_five/product/product.png')}
      />
      <ContainerInfo>
        <Title>Calça de malha texturizada com faixa - Laranja Pierre</Title>
        <Ref>REF: 59.12.0258_6012</Ref>
        <Price>R$ 529</Price>
        <Selector
          onSelectedItemsChange={() => {}}
          items={sex}
          selectText="M:"
        />
        <ContainerIcon onPress={() => setMarked(!marked)}>
          <Icon name={marked ? 'bookmark' : 'bookmark-o'} size={iconSize} />
        </ContainerIcon>
        <Delete>EXCLUIR</Delete>
      </ContainerInfo>
    </Container>
  );
};

export default Product;
