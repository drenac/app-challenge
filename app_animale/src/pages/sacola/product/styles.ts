import styled from 'styled-components/native';
import {Dimensions, StyleSheet} from 'react-native';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

const {height, width} = Dimensions.get('window');

export const iconSize = height * 0.035;

export const Container = styled.View`
  flex-direction: row;
  align-self: center;
  margin-top: 30px;
  width: 90%;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
`;

export const Image = styled.Image`
  width: 128px;
  height: 192px;
`;

export const ContainerInfo = styled.View`
  padding: 20px;
  margin-bottom: 50px;
  flex: 1;
`;

export const Title = styled.Text`
  font-size: ${height * 0.02}px;
  font-family: 'Futura BT';
  margin-top: 20px;
`;

export const Ref = styled.Text`
  font-size: ${height * 0.02}px;
  font-family: 'Futura Bk BT';
  margin-top: 10px;
`;

export const Price = styled.Text`
  font-size: ${height * 0.02}px;
  font-family: 'Futura BT';
  margin-top: 14px;
`;

export const Delete = styled.Text`
  font-size: ${height * 0.018}px;
  font-family: 'Futura Bk BT';
  position: absolute;
  right: 0;
  padding: 4px;
`;

export const Selector = styled(SectionedMultiSelect).attrs({
  uniqueKey: 'id',
  single: true,
  showChips: false,
  hideSearch: true,
  hideConfirm: true,
  alwaysShowSelectText: true,
  showCancelButton: false,
  styles: {
    selectToggle: {
      width: width * 0.3,
      backgroundColor: '#f3f3f3',
      padding: 15,
    },
    container: {
      padding: 20,
      flex: 0,
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    modalWrapper: {
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
})``;

export const ContainerIcon = styled.TouchableOpacity`
  position: absolute;
  padding: 6px;
  bottom: 0;
  right: 0;
`;
