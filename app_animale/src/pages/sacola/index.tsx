import React from 'react';

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import {
  Container,
  ContainerTotal,
  ProductCount,
  Total,
  ContainerButton,
  ContainerIcon,
  TextButton,
  ContainerButtonGift,
  iconSize,
} from './styles';

import Header from '../../components/header';
import Product from './product';
import {ScrollView} from 'react-native';
import Cupons from './cupons';
import Entrega from './entrega';
import Resumo from './resumo';

import useSacola from './useSacola';

const Sacola: React.FC = () => {
  const {changeToMenu} = useSacola();

  return (
    <Container>
      <ScrollView>
        <Header title="SACOLA" icon="close" onPressAction={changeToMenu} />
        <ContainerTotal>
          <ProductCount>2 produtos</ProductCount>
          <Total>Subtotal R$ 1.058</Total>
        </ContainerTotal>
        <Product />
        <Product />
        <ContainerButtonGift>
          <ContainerButton>
            <ContainerIcon>
              <Feather name="gift" size={iconSize} />
            </ContainerIcon>
            <TextButton>Enviar para presente?</TextButton>
            <SimpleLineIcons name="arrow-down" />
          </ContainerButton>
        </ContainerButtonGift>
        <Cupons />
        <Entrega />
        <Resumo />
      </ScrollView>
    </Container>
  );
};

export default Sacola;
