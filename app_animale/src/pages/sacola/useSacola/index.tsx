import {useNavigation} from '@react-navigation/native';

const useSacola = () => {
  const {navigate} = useNavigation();

  function changeToMenu() {
    navigate('Menu');
  }

  return {
    changeToMenu,
  };
};

export default useSacola;
