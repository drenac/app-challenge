import React from 'react';

import {
  Container,
  TextButton,
  Title,
  ContainerInput,
  ContainerText,
  Input,
} from './styles';

const Cupons: React.FC = () => {
  return (
    <Container>
      <Title>CÓDIGOS DE VENDEDOR E CUPONS</Title>
      <ContainerInput>
        <Input placeholder="Código da vendedora, cupom…" />
        <ContainerText>
          <TextButton>Aplicar</TextButton>
        </ContainerText>
      </ContainerInput>
    </Container>
  );
};

export default Cupons;
