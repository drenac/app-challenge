import styled from 'styled-components/native';
import {Dimensions, StyleSheet} from 'react-native';

const {height} = Dimensions.get('window');

export const Container = styled.View`
  padding: 40px 20px;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
`;

export const Title = styled.Text`
  font-size: ${height * 0.02}px;
  font-family: 'Futura BT';
`;

export const ContainerInput = styled.View`
  margin-top: 20px;
  border-width: 1px;
  flex-direction: row;
  padding-left: 15px;
`;

export const Input = styled.TextInput`
  font-size: ${height * 0.02}px;
  flex: 1;
`;

export const ContainerText = styled.TouchableOpacity`
  padding: 0 15px;
  align-items: center;
  justify-content: center;
`;

export const TextButton = styled.Text`
  font-size: ${height * 0.02}px;
  font-family: 'Futura Bk BT';
`;
