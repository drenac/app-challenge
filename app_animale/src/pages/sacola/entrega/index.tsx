import React from 'react';

import {
  Container,
  TextButton,
  Title,
  ContainerInput,
  ContainerText,
  Input,
} from './styles';

const Entrega: React.FC = () => {
  return (
    <Container>
      <Title>ENTREGA</Title>
      <ContainerInput>
        <Input placeholder="Digite o CEP da entrega" />
        <ContainerText>
          <TextButton>Calcular</TextButton>
        </ContainerText>
      </ContainerInput>
    </Container>
  );
};

export default Entrega;
