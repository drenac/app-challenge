import styled from 'styled-components/native';
import {Dimensions, StyleSheet} from 'react-native';

const {height} = Dimensions.get('window');

export const Container = styled.View`
  background-color: #f3f3f3;
  padding: 30px 20px 25px;
`;

export const Title = styled.Text`
  font-size: ${height * 0.024}px;
  font-family: 'Futura BT';
`;

export const ContainerText = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 20px;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
`;

export const TextSmall = styled.Text`
  font-size: ${height * 0.02}px;
  font-family: 'Futura BK BT';
`;

export const TextBold = styled.Text`
  font-size: ${height * 0.02}px;
  font-family: 'Futura BT';
`;

export const ContainerButton = styled.TouchableOpacity`
  background-color: black;
  align-items: center;
  padding: 18px 0;
`;

export const TextButton = styled.Text`
  color: white;
  font-size: ${height * 0.02}px;
  letter-spacing: 1px;
  font-family: 'Futura BT';
`;
