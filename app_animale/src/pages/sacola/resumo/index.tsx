import React from 'react';

import {
  Container,
  TextButton,
  Title,
  ContainerButton,
  TextBold,
  TextSmall,
  ContainerText,
} from './styles';

const Resumo: React.FC = () => {
  return (
    <Container>
      <Title>RESUMO DO PEDIDO</Title>
      <ContainerText>
        <TextSmall>SubTotal</TextSmall>
        <TextSmall>R$ 1.058</TextSmall>
      </ContainerText>
      <ContainerText>
        <TextSmall>Entrega</TextSmall>
        <TextSmall>R$ 6.48</TextSmall>
      </ContainerText>
      <ContainerText>
        <TextSmall>Total</TextSmall>
        <TextBold>R$ 1.064,48</TextBold>
      </ContainerText>
      <ContainerButton>
        <TextButton>COMPRAR</TextButton>
      </ContainerButton>
    </Container>
  );
};

export default Resumo;
