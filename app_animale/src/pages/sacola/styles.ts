import styled from 'styled-components/native';
import {Dimensions, StyleSheet} from 'react-native';

const {height} = Dimensions.get('window');

export const iconSize = height * 0.03;

export const Container = styled.View`
  flex: 1;
  background-color: white;
`;

export const ContainerTotal = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 20px;
  border-top-width: ${StyleSheet.hairlineWidth}px;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
`;

export const ProductCount = styled.Text`
  font-size: ${height * 0.022}px;
  font-family: 'Futura Md  BT';
`;

export const Total = styled.Text`
  font-size: ${height * 0.022}px;
  font-family: 'Futura BT';
`;

export const ContainerButtonGift = styled.View`
  border-width: ${StyleSheet.hairlineWidth}px;
  border-bottom-width: 0;

  align-items: center;
`;

export const ContainerButton = styled.TouchableOpacity.attrs({
  onPress: () => {},
})`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
`;

export const ContainerIcon = styled.View`
  margin-right: 20px;
`;

export const TextButton = styled.Text`
  flex: 1;
  font-family: 'Futura BT';
  font-size: ${height * 0.022}px;
`;
