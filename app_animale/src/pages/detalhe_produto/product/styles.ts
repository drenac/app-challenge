import styled from 'styled-components/native';
import {Dimensions, StatusBar} from 'react-native';
const {height} = Dimensions.get('window');

interface Props {
  needMargin?: boolean;
}

export const Container = styled.View``;
export const iconSize = height * 0.03;
export const ImageHome = styled.ImageBackground.attrs({
  source: require('../../../assets/page_four/product/product.png'),
  resizeMode: 'stretch',
})`
  height: 560px;
  position: relative;
  width: 100%;
  justify-content: flex-end;
  align-items: center;
`;

export const ContainerIconBag = styled.View`
  position: absolute;
  right: 16px;
  top: ${StatusBar.currentHeight ? StatusBar.currentHeight + 10 : 48}px;
  height: 150px;
  justify-content: space-between;
  align-items: center;
`;

export const IconBag = styled.TouchableOpacity`
  padding: 6px;
`;

export const ContainerIconBack = styled.TouchableOpacity`
  position: absolute;
  top: ${StatusBar.currentHeight ? StatusBar.currentHeight + 10 : 48}px;
  height: 150px;
  justify-content: space-between;
  align-items: center;
  left: 20px;
  align-items: flex-start;
`;

export const Title = styled.Text`
  text-align: center;
  margin-top: 14px;
  font-family: 'Futura Bk BT';
  font-size: ${height * 0.02}px;
`;

export const Price = styled.Text`
  text-align: center;
  margin-top: 14px;
  font-family: 'Futura Bk BT';
  font-weight: bold;
  font-size: ${height * 0.02}px;
`;

export const ContainerButtonBag = styled.TouchableOpacity`
  width: 90%;
  background-color: black;
  align-self: center;
  align-items: center;
  padding: 16px;
  margin-top: 30px;
  flex-direction: row;
  justify-content: center;
`;

export const TextButtonBag = styled.Text`
  color: white;
  font-family: 'Futura BT';
  margin-left: 12px;
`;

export const ContainerLooking = styled.TouchableOpacity`
  margin-top: 30px;
  align-self: center;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const TextLooking = styled.Text`
  font-family: 'Futura Md BT';
  font-size: ${height * 0.024}px;
  margin-left: 20px;
`;

export const ContainerImages = styled.View`
  flex-direction: row;
  align-self: center;
`;

export const Image = styled.Image<Props>`
  margin-left: ${(props) => (props.needMargin ? 20 : 0)}px;
  align-self: center;
  margin-top: 24px;
`;

export const ImageBig = styled(Image)<Props>`
  width: 85%;
`;
