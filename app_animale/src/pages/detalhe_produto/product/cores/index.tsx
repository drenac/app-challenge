import React from 'react';

import {Container} from './styles';
import {FlatList} from 'react-native';

import Item from './items';

import TituloCategoria from '../../../../components/titulo_categoria';

const Data = [
  {
    img: require('../../../../assets/page_four/product_details4/product_details.png'),
  },
  {
    img: require('../../../../assets/page_four/product_details5/product_details.png'),
  },
  {
    img: require('../../../../assets/page_four/product_details5/product_details.png'),
  },
];

const Cores: React.FC = () => {
  return (
    <Container>
      <TituloCategoria title={'CORES DISPONÍVEIS'} />
      <FlatList
        contentContainerStyle={{paddingRight: 20}}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={Data}
        renderItem={({item}) => <Item {...item} />}
        keyExtractor={(item, index) => String(index)}
      />
    </Container>
  );
};

export default Cores;
