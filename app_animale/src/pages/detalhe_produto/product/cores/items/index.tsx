import React from 'react';

import {Container, Image} from './styles';
import {ImageSourcePropType} from 'react-native';

interface Props {
  img: ImageSourcePropType;
}

const Item: React.FC<Props> = ({img}) => {
  return (
    <Container>
      <Image source={img} />
    </Container>
  );
};

export default Item;
