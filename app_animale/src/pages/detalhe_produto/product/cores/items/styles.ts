import styled from 'styled-components/native';

export const Container = styled.View`
  margin-left: 20px;
`;

export const Image = styled.Image.attrs({resizeMode: 'stretch'})`
  height: 256px;
  width: 196px;
`;
