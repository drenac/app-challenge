import styled from 'styled-components/native';
import {Dimensions, StyleSheet} from 'react-native';

const {height} = Dimensions.get('window');

interface Props {
  line?: boolean;
}

export const Container = styled.View`
  width: 90%;
  background-color: #f3f3f3;
  align-self: center;
  margin-top: 24px;
  padding: 20px;
`;

export const TextRef = styled.Text`
  font-family: 'Futura Bk BT';
  font-size: ${height * 0.022}px;
`;

export const Text = styled.Text`
  font-family: 'Futura Bk BT';
  font-size: ${height * 0.024}px;
`;

export const ContainerButton = styled.TouchableOpacity.attrs({
  onPress: () => {},
})<Props>`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 20px 0;
  border-bottom-width: ${(props) =>
    props.line ? StyleSheet.hairlineWidth : 0}px;
`;

export const TextButton = styled.Text`
  flex: 1;
  font-family: 'Futura Bk BT';
  font-size: ${height * 0.022}px;
`;
