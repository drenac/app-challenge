import React from 'react';

import {Container, Text, TextRef, ContainerButton, TextButton} from './styles';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

const Detalhe: React.FC = () => {
  return (
    <Container>
      <TextRef>{'REF: 59.12.0258_6012\n\n'}</TextRef>
      <Text>
        A calça de malha texturizada vermelha tem shape justo, cintura e
        tornozelos finalizados por elástico, comprimento cropped e cinto estilo
        paraquedista. Combine com botas de cano curto e camisas de seda para um
        look chic, ou use com tênis e camisetas básicas para uma pegada urbana.
      </Text>
      <ContainerButton line>
        <TextButton>Informações sobre a peça</TextButton>
        <SimpleLineIcons name="arrow-right" />
      </ContainerButton>
      <ContainerButton line>
        <TextButton>Cuidados com o produto</TextButton>
        <SimpleLineIcons name="arrow-right" />
      </ContainerButton>
      <ContainerButton>
        <TextButton>Devolução e troca fácil</TextButton>
        <SimpleLineIcons name="arrow-right" />
      </ContainerButton>
    </Container>
  );
};

export default Detalhe;
