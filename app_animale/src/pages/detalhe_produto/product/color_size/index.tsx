import React from 'react';

import {Container, Selector} from './styles';

const cores = [
  {
    name: 'Cor : Laranja',
    id: 10,
  },
  {
    name: 'Cor : Azul',
    id: 17,
  },
  {
    name: 'Cor : Cinza',
    id: 13,
  },
  {
    name: 'Cor : Preto',
    id: 14,
  },
];
const tamanhos = [
  {
    name: 'Tamanho P',
    id: 10,
  },
  {
    name: 'Tamanho M',
    id: 17,
  },
  {
    name: 'Tamanho G',
    id: 13,
  },
  {
    name: 'Tamanho GG',
    id: 14,
  },
];

const ColorSize: React.FC = () => {
  return (
    <Container>
      <Selector
        onSelectedItemsChange={() => {}}
        items={cores}
        selectText="Cor:"
      />
      <Selector
        onSelectedItemsChange={() => {}}
        items={tamanhos}
        selectText="Tamanho:"
      />
    </Container>
  );
};

export default ColorSize;
