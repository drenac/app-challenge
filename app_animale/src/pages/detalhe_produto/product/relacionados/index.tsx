import React from 'react';

import {Container} from './styles';
import {FlatList} from 'react-native';

import Item from './items';

import TituloCategoria from '../../../../components/titulo_categoria';

const Data = [
  {
    title: 'Camisa de seda parka mi…',
    price: '529',
    img: require('../../../../assets/page_one/modelo2/modelo2.png'),
  },
  {
    title: 'Camisa de seda parka mi…',
    price: '529',
    img: require('../../../../assets/page_one/modelo1/modelo1.png'),
  },
];

const Relacionados: React.FC = () => {
  return (
    <Container>
      <TituloCategoria title={'PRODUTOS RELACIONADOS'} />
      <FlatList
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{paddingRight: 20}}
        horizontal
        data={Data}
        renderItem={({item}) => <Item {...item} />}
        keyExtractor={(item, index) => String(index)}
      />
    </Container>
  );
};

export default Relacionados;
