import React from 'react';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {
  Container,
  ImageHome,
  ContainerIconBag,
  iconSize,
  ContainerIconBack,
  IconBag,
  Title,
  Price,
  ContainerButtonBag,
  TextButtonBag,
  ContainerLooking,
  TextLooking,
  ContainerImages,
  Image,
  ImageBig,
} from './styles';
import ColorSize from './color_size';
import Detalhe from './detalhe';
import Cores from './cores';
import useProduct from '../useProduct';

import Relacionados from './relacionados';

const Product: React.FC = () => {
  const {changeToProducts, changeToSacola} = useProduct();

  return (
    <Container>
      <ImageHome>
        <ContainerIconBack onPress={changeToProducts}>
          <SimpleLineIcons name="arrow-left" color="black" size={iconSize} />
        </ContainerIconBack>
        <ContainerIconBag>
          <IconBag>
            <SimpleLineIcons name="handbag" color="black" size={iconSize} />
          </IconBag>
          <IconBag>
            <Ionicons
              name="md-bookmark-outline"
              color="black"
              size={iconSize * 1.2}
            />
          </IconBag>
          <IconBag>
            <Ionicons
              name="md-share-outline"
              color="black"
              size={iconSize * 1.2}
            />
          </IconBag>
        </ContainerIconBag>
      </ImageHome>
      <Title>Calça de malha texturizada com faixa</Title>
      <Price>R$ 529</Price>
      <ColorSize />
      <ContainerButtonBag onPress={changeToSacola}>
        <SimpleLineIcons name="handbag" color="white" size={iconSize * 0.8} />
        <TextButtonBag>ADICIONAR Á SACOLA</TextButtonBag>
      </ContainerButtonBag>
      <ContainerLooking>
        <SimpleLineIcons name="location-pin" size={iconSize * 0.9} />
        <TextLooking>Encontrar na loja...</TextLooking>
      </ContainerLooking>
      <Detalhe />
      <ContainerImages>
        <Image
          source={require('../../../assets/page_four/product_details1/product_details.png')}
        />
        <Image
          needMargin
          source={require('../../../assets/page_four/product_details2/product_details.png')}
        />
      </ContainerImages>
      <ImageBig
        source={require('../../../assets/page_four/product_details3/product_details.png')}
      />
      <Cores />
      <Relacionados />
    </Container>
  );
};

export default Product;
