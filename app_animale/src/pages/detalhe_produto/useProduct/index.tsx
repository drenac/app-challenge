import {useNavigation} from '@react-navigation/native';

const useProduct = () => {
  const {navigate} = useNavigation();

  function changeToProducts() {
    navigate('Menu');
  }

  function changeToSacola() {
    navigate('Sacola');
  }

  return {changeToProducts, changeToSacola};
};

export default useProduct;
