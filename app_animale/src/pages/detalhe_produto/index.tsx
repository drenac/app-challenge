import React from 'react';

import {Container} from './styles';
import Rodape from '../../components/rodape';
import Product from './product';
import {ScrollView, StatusBar} from 'react-native';

const DetalheProduto: React.FC = () => {
  return (
    <Container>
      <StatusBar backgroundColor="transparent" translucent />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Product />
        <Rodape />
      </ScrollView>
    </Container>
  );
};

export default DetalheProduto;
