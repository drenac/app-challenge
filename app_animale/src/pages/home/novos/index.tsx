import React from 'react';

import {Container} from './styles';
import {FlatList} from 'react-native';

import Item from './items';

import TituloCategoria from '../../../components/titulo_categoria';

const Data = [
  {
    title: 'HOT TRENDS',
    img: require('../../../assets/page_one/lado2/Rectangle.png'),
  },
  {
    title: 'JAQUETAS',
    img: require('../../../assets/page_one/lado1/Rectangle.png'),
  },
];

const Novos: React.FC = () => {
  return (
    <Container>
      <TituloCategoria title={'NEW IN'} textButton={'Ver Tudo'} />
      <FlatList
        contentContainerStyle={{paddingRight: 20}}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={Data}
        renderItem={({item}) => <Item {...item} />}
        keyExtractor={(item, index) => String(index)}
      />
    </Container>
  );
};

export default Novos;
