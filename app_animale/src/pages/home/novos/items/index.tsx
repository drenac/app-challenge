import React from 'react';

import {
  Container,
  TextButton,
  Title,
  ContainerButton,
  Image,
  ContainerText,
} from './styles';
import {ImageSourcePropType} from 'react-native';

interface Props {
  title: string;
  img: ImageSourcePropType;
}

const Item: React.FC<Props> = ({title, img}) => {
  return (
    <Container>
      <Image source={img} />
      <ContainerText>
        <Title>{title}</Title>
        <ContainerButton>
          <TextButton>SHOW NOW</TextButton>
        </ContainerButton>
      </ContainerText>
    </Container>
  );
};

export default Item;
