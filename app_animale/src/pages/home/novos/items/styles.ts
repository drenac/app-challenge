import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

export const Container = styled.View`
  margin-left: 20px;
`;

export const Image = styled.Image.attrs({resizeMode: 'stretch'})`
  height: 256px;
  width: 196px;
`;

export const ContainerText = styled.View`
  position: absolute;
  height: 100%;
  width: 100%;
  background-color: #00000040;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.Text`
  color: white;
  font-size: ${height * 0.02}px;
  font-family: 'Futura Md BT';
`;

export const ContainerButton = styled.TouchableOpacity`
  width: 70%;
  padding: 2px 0px;
  border-width: 1px;
  border-color: white;
  margin-top: 8px;
  align-items: center;
`;

export const TextButton = styled.Text`
  color: white;
  font-size: ${height * 0.018}px;
  font-family: 'Futura Md BT';
`;
