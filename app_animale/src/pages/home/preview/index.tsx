import React from 'react';

import {
  Container,
  TextButton,
  ContainerButton,
  ContainerText,
  Image,
} from './styles';

const Preview: React.FC = () => {
  return (
    <Container>
      <Image source={require('../../../assets/page_one/secao/Group7.png')} />
      <ContainerText>
        <ContainerButton>
          <TextButton>SHOP NOW</TextButton>
        </ContainerButton>
      </ContainerText>
    </Container>
  );
};

export default Preview;
