import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

export const Container = styled.View`
  height: 424px;
  margin-top: 40px;
  margin-left: 20px;
`;

export const Image = styled.Image.attrs({resizeMode: 'stretch'})`
  height: 424px;
  width: ${width * 0.9}px;
`;

export const ContainerText = styled.View`
  position: absolute;
  height: 100%;
  width: 100%;
  align-items: center;
  justify-content: center;
`;

export const ContainerButton = styled.TouchableOpacity`
  width: 50%;
  padding: 10px 0px;
  border-width: 1px;
  border-color: white;
  margin-top: 8px;
  align-items: center;
  margin-top: 80px;
`;

export const TextButton = styled.Text`
  color: white;
  font-size: ${height * 0.018}px;
  font-family: 'Futura Md BT';
`;
