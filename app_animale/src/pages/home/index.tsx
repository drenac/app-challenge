import React from 'react';

import {Container} from './styles';

import {StatusBar, ScrollView} from 'react-native';

import Editorial from './editorial';
import Novos from './novos';
import VistoRecente from './visto_recente';
import PodeGostar from './pode_gostar';
import Preview from './preview';
import Rodape from '../../components/rodape';
import {Api} from '../../api';
import {useEffect} from 'react';
import useItens from '../../context/useItems';

const Home: React.FC = () => {
  const {changeItens} = useItens();

  function setItens() {
    changeItens(Api);
  }

  useEffect(() => {
    setItens();
  }, []);

  return (
    <Container>
      <ScrollView>
        <StatusBar translucent backgroundColor="transparent" />
        <Editorial />
        <Novos />
        <Preview />
        <VistoRecente />
        <PodeGostar />
        <Rodape />
      </ScrollView>
    </Container>
  );
};

export default Home;
