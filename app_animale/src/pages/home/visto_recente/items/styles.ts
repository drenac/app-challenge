import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

export const Container = styled.View`
  margin-left: 20px;
`;

export const Image = styled.Image.attrs({resizeMode: 'stretch'})`
  height: 281px;
  width: 188px;
`;

export const ContainerText = styled.View`
  margin-top: 20px;
  margin-bottom: 20px;
`;

export const Name = styled.Text`
  color: black;
  font-size: ${height * 0.02}px;
  font-family: 'Futura Bk BT';
`;

export const Price = styled.Text`
  color: black;
  font-weight: bold;
  font-size: ${height * 0.02}px;
  font-family: 'Futura Bk BT';
`;

export const Parcelado = styled.Text`
  color: black;
  font-size: ${height * 0.02}px;
  font-family: 'Futura Bk BT';
`;
