import React from 'react';

import {
  Container,
  Image,
  ContainerText,
  Name,
  Parcelado,
  Price,
} from './styles';
import {ImageSourcePropType} from 'react-native';

interface Props {
  title: string;
  price: string;
  img: ImageSourcePropType;
}

const Item: React.FC<Props> = ({title, img, price}) => {
  return (
    <Container>
      <Image source={img} />
      <ContainerText>
        <Name>{title}</Name>
        <Price>R$ {price}</Price>
        <Parcelado>5x de R$ 105,80</Parcelado>
      </ContainerText>
    </Container>
  );
};

export default Item;
