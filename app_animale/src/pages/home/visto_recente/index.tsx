import React from 'react';

import {Container} from './styles';
import {FlatList} from 'react-native';

import Item from './items';

import TituloCategoria from '../../../components/titulo_categoria';

const Data = [
  {
    title: 'Camisa de seda parka mi…',
    price: '529',
    img: require('../../../assets/page_one/modelo4/modelo4.png'),
  },
  {
    title: 'Camisa de seda parka mi…',
    price: '529',
    img: require('../../../assets/page_one/modelo3/modelo3.png'),
  },
];

const VistoRecente: React.FC = () => {
  return (
    <Container>
      <TituloCategoria title={'VISTOS RECENTEMENTE'} textButton={'Limpar'} />
      <FlatList
        contentContainerStyle={{paddingRight: 20}}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={Data}
        renderItem={({item}) => <Item {...item} />}
        keyExtractor={(item, index) => String(index)}
      />
    </Container>
  );
};

export default VistoRecente;
