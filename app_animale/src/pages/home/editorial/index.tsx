import React from 'react';
import IconBag from 'react-native-vector-icons/SimpleLineIcons';

import {
  Container,
  ImageHome,
  ContainerIconBag,
  iconSize,
  TitleBig,
  TitleSmall,
  ContainerButton,
  TextButton,
  ContainerDots,
  Dots,
} from './styles';

const Editorial: React.FC = () => {
  return (
    <Container>
      <ImageHome>
        <ContainerIconBag onPress={() => {}}>
          <IconBag name="handbag" color="white" size={iconSize} />
        </ContainerIconBag>
        <TitleSmall>PREVIEW</TitleSmall>
        <TitleBig>NEW WAVE</TitleBig>
        <ContainerButton>
          <TextButton>SHOP NOW</TextButton>
        </ContainerButton>
        <ContainerDots>
          <Dots />
          <Dots selected />
          <Dots />
          <Dots />
          <Dots />
        </ContainerDots>
      </ImageHome>
    </Container>
  );
};

export default Editorial;
