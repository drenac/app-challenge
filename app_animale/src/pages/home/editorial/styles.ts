import styled from 'styled-components/native';
import {Dimensions, StatusBar} from 'react-native';
const {height} = Dimensions.get('window');

interface Props {
  selected?: boolean;
}

export const Container = styled.View``;
export const iconSize = height * 0.03;
export const ImageHome = styled.ImageBackground.attrs({
  source: require('../../../assets/page_one/entrada/editorial_pic.png'),
  resizeMode: 'stretch',
})`
  height: 536px;
  position: relative;
  width: 100%;
  justify-content: flex-end;
  align-items: center;
`;

export const ContainerIconBag = styled.TouchableOpacity`
  position: absolute;
  right: 20px;
  top: ${StatusBar.currentHeight ? StatusBar.currentHeight + 5 : 48}px;
`;

export const TitleSmall = styled.Text`
  color: white;
  font-size: ${height * 0.0258}px;
  font-family: Didot;
  letter-spacing: 2.8px;
`;
export const TitleBig = styled(TitleSmall)`
  font-size: ${height * 0.0458}px;
`;

export const ContainerButton = styled.TouchableOpacity`
  width: 200px;
  height: 38px;
  border-width: 2px;
  border-color: white;
  margin-top: 40px;
  align-items: center;
  justify-content: center;
`;
export const TextButton = styled.Text`
  color: white;
  font-family: 'Futura Md BT';
  font-size: ${height * 0.02}px;
`;

export const ContainerDots = styled.View`
  width: 240px;
  flex-direction: row;
  margin-top: 48px;
  margin-bottom: 16px;
  align-items: center;
`;

export const Dots = styled.View<Props>`
  background-color: white;
  flex: 1;
  height: ${(props) => (props.selected ? '2.4px' : '1px')};
  margin: 0 2px;
`;
