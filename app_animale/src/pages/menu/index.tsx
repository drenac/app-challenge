import React from 'react';

import {Container} from './styles';
import {ScrollView} from 'react-native-gesture-handler';

import useMenu from './useMenu';

import HeaderSearch from './menu_search';

import Header from '../../components/header';
import Input from './components/input';
import TiposHorizontais from './components/tipos_horizontal';
import {StatusBar} from 'react-native';
import BotaoMenuSimples from './components/botao/menu_simples';
import BotaoMenuVariantOne from './components/botao/menu_variant_1';
import BotaoMenuVariantTwo from './components/botao/menu_variant_2';

const Menu: React.FC = () => {
  const {selectedOption, setSelectedOption} = useMenu();

  function backScreen() {
    setSelectedOption(false);
  }

  if (selectedOption) {
    return <HeaderSearch backScreen={backScreen} />;
  }

  return (
    <Container>
      <ScrollView showsVerticalScrollIndicator={false}>
        <StatusBar barStyle="dark-content" translucent />
        <Header />
        <Input />
        <TiposHorizontais />
        <BotaoMenuSimples
          onPress={() => setSelectedOption(true)}
          title="COLEÇÃO"
          img={require('../../assets/page_two/button1/button.png')}
        />
        <BotaoMenuSimples
          onPress={() => setSelectedOption(true)}
          title="NOVIDADES"
          img={require('../../assets/page_two/button2/button.png')}
        />
        <BotaoMenuSimples
          onPress={() => setSelectedOption(true)}
          title="ACESSÓRIOS"
          img={require('../../assets/page_two/button3/button.png')}
        />
        <BotaoMenuSimples
          onPress={() => setSelectedOption(true)}
          title="INTIMATES"
          img={require('../../assets/page_two/button4/button.png')}
        />
        <BotaoMenuSimples
          onPress={() => setSelectedOption(true)}
          title="SALE"
          img={require('../../assets/page_two/button5/button.png')}
        />
        <BotaoMenuVariantOne
          title="ANIMALE ORO"
          img={require('../../assets/page_two/button6/button.png')}
        />
        <BotaoMenuVariantTwo
          img={require('../../assets/page_two/button7/button.png')}
        />
      </ScrollView>
    </Container>
  );
};

export default Menu;
