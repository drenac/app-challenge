import {useState} from 'react';

const useMenu = () => {
  const [selectedOption, setSelectedOption] = useState(false);

  return {
    selectedOption,
    setSelectedOption,
  };
};

export default useMenu;
