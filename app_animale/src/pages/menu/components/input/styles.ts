import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

export const iconSize = height * 0.025;

export const Container = styled.View`
  align-items: center;
  margin-top: 24px;
`;

export const ContainerInput = styled.View`
  width: 90%;
  background-color: #f3f3f3;
  flex-direction: row;
  align-items: center;
  border-radius: 5px;
  padding: 0 15px;
`;

export const TextInput = styled.TextInput.attrs({
  placeholder: 'Busque por produtos...',
})`
  margin-left: 10px;
  font-size: ${height * 0.02}px;
`;
