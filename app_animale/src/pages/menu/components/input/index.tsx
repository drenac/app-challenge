import React from 'react';

import {Container, TextInput, ContainerInput, iconSize} from './styles';
import Icon from 'react-native-vector-icons/Feather';

const Input: React.FC = () => {
  return (
    <Container>
      <ContainerInput>
        <Icon name="search" size={iconSize} />
        <TextInput />
      </ContainerInput>
    </Container>
  );
};

export default Input;
