import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

export const Container = styled.View`
  margin-left: 20px;
  align-items: center;
`;

export const Image = styled.Image`
  width: ${height * 0.09}px;
  height: ${height * 0.09}px;
`;

export const Title = styled.Text`
  font-size: ${height * 0.024}px;
  font-family: 'Futura Bk BT';
`;
