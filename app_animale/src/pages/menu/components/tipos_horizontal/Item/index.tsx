import React from 'react';

import {Container, Title, Image} from './styles';

interface Props {
  title: string;
  img: string;
}

const Item: React.FC<Props> = ({title, img}) => {
  return (
    <Container>
      <Image source={img} />
      <Title>{title}</Title>
    </Container>
  );
};

export default Item;
