import React from 'react';

import {Container} from './styles';
import {FlatList} from 'react-native-gesture-handler';
import Item from './Item';

const data = [
  {
    img: require('../../../../assets/page_two/story1/Oval.png'),
    title: 'Malha',
  },
  {
    img: require('../../../../assets/page_two/story2/Oval.png'),
    title: 'Jaquetas',
  },
  {
    img: require('../../../../assets/page_two/story3/Oval.png'),
    title: 'Tricot',
  },
  {
    img: require('../../../../assets/page_two/story4/Oval.png'),
    title: 'Blusas',
  },
  {
    img: require('../../../../assets/page_two/story5/Oval.png'),
    title: 'Shorts',
  },
];

const TiposHorizontal: React.FC = () => {
  return (
    <Container>
      <FlatList
        contentContainerStyle={{paddingRight: 20}}
        showsHorizontalScrollIndicator={false}
        data={data}
        horizontal
        renderItem={({item}) => <Item {...item} />}
        keyExtractor={(item, index) => String(index)}
      />
    </Container>
  );
};

export default TiposHorizontal;
