import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

export const Container = styled.TouchableOpacity`
  background-color: #c1c3c2;
  width: 90%;
  align-self: center;
  margin-top: 20px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const ContainerTitle = styled.View`
  position: absolute;
  right: 28px;
`;

export const Title = styled.Text`
  color: white;
  font-size: ${height * 0.022}px;
  font-family: 'Futura Md BT';
`;

export const Image = styled.Image`
  width: 100%;
`;
