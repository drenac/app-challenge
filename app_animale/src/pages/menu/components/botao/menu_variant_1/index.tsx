import React from 'react';

import {Container, Image, Title, ContainerTitle} from './styles';
import {ImageSourcePropType} from 'react-native';

interface Props {
  title: string;
  img: ImageSourcePropType;
}

const ButtonMenuVariantOne: React.FC<Props> = ({title, img}) => {
  return (
    <Container>
      <Image source={img} />
      <ContainerTitle>
        <Title>{title}</Title>
      </ContainerTitle>
    </Container>
  );
};

export default ButtonMenuVariantOne;
