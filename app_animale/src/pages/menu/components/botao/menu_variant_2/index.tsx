import React from 'react';

import {Container, Image, Title} from './styles';
import {ImageSourcePropType} from 'react-native';

interface Props {
  img: ImageSourcePropType;
}

const BotaoMenuSimples: React.FC<Props> = ({img}) => {
  return (
    <Container>
      <Image source={img} />
    </Container>
  );
};

export default BotaoMenuSimples;
