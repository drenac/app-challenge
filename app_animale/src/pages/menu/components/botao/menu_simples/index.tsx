import React from 'react';

import {Container, Image, Title} from './styles';
import {ImageSourcePropType} from 'react-native';

interface Props {
  title: string;
  img: ImageSourcePropType;
  onPress: () => void;
}

const BotaoMenuSimples: React.FC<Props> = ({title, img, onPress}) => {
  return (
    <Container onPress={onPress}>
      <Title>{title}</Title>
      <Image source={img} />
    </Container>
  );
};

export default BotaoMenuSimples;
