import React from 'react';

import {Container} from './styles';

import Header from '../../../components/header';
import FilterSort from './components/filter_sort';
import ListagemProdutos from './components/listagem_produtos';
import useMenuSearch from './useMenuSearch';

interface Props {
  backScreen: () => void;
}

const MenuSearch: React.FC<Props> = ({backScreen}) => {
  const {filteredList, changeFilter} = useMenuSearch();

  return (
    <Container>
      <Header title="VESTIDO" backButton onPressBack={backScreen} />
      <FilterSort onChangeText={changeFilter} />
      <ListagemProdutos data={filteredList} />
    </Container>
  );
};

export default MenuSearch;
