import {useState, useRef} from 'react';
import {useNavigation} from '@react-navigation/native';
import useItens from '../../../../context/useItems';

interface List {
  name: string;
  image: string;
}

const useMenuSearch = () => {
  const {itens} = useItens();
  const list = useRef<Array<List>>(itens).current;
  const [filteredList, setFilteredList] = useState<Array<List>>(itens);
  const [singleItem, setSingleItem] = useState(false);
  const {navigate} = useNavigation();

  function changeToDetails() {
    navigate('DetalheProduto');
  }

  function changeFilter(text: string) {
    setFilteredList(
      list.filter((item) =>
        item.name.toLowerCase().includes(text.toLowerCase()),
      ),
    );
  }

  return {
    singleItem,
    setSingleItem,
    changeToDetails,
    filteredList,
    changeFilter,
  };
};

export default useMenuSearch;
