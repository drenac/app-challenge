import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height} = Dimensions.get('window');

export const iconSize = height * 0.025;

export const Container = styled.View``;

export const ContainerHeader = styled.View`
  flex-direction: row;
  width: 90%;
  align-self: center;
  justify-content: space-between;
  margin-top: 28px;
  align-items: center;
`;

export const TextCount = styled.Text`
  font-size: ${height * 0.022}px;
  font-family: 'Futura Bk BT';
`;

export const ContainerIcon = styled.TouchableOpacity`
  flex-direction: row;
  width: 14%;
  padding: 6px 0;
  justify-content: space-between;
  align-items: center;
`;
