import React from 'react';
import {
  Container,
  ContainerIcon,
  ContainerHeader,
  TextCount,
  iconSize,
} from './styles';
import IconMultiple from 'react-native-vector-icons/AntDesign';
import IconSingle from 'react-native-vector-icons/Entypo';
import useMenuSearch from '../../../useMenuSearch';

const Header: React.FC = () => {
  const {setSingleItem, singleItem} = useMenuSearch();
  return (
    <Container>
      <ContainerHeader>
        <TextCount>200 Produtos</TextCount>
        <ContainerIcon onPress={() => setSingleItem(!singleItem)}>
          <IconMultiple
            name="appstore1"
            size={iconSize}
            color={!singleItem ? 'black' : 'gray'}
          />
          <IconSingle
            name="controller-stop"
            size={iconSize * 1.5}
            color={singleItem ? 'black' : 'gray'}
          />
        </ContainerIcon>
      </ContainerHeader>
    </Container>
  );
};

export default Header;
