import React from 'react';

import {Container} from './styles';
import Header from './header';
import Item from './Item';
import {FlatList} from 'react-native-gesture-handler';
import useMenuSearch from '../../useMenuSearch';

// const data = [
//   {
//     img: require('../../../../../assets/page_three/Item2/produto.png'),
//     type: 'Sale',
//     title: 'Camisa de seda parka mi…',
//     price: 'R$ 529',
//     oldPrice: 'R$ 898',
//     parcelado: '5x de R$ 105,80',
//   },
//   {
//     img: require('../../../../../assets/page_three/Item4/produto.png'),
//     type: 'preview | +2 cores',
//     title: 'Camisa de seda parka mi…',
//     price: 'R$ 529',
//     oldPrice: '',
//     parcelado: '5x de R$ 105,80',
//   },
//   {
//     img: require('../../../../../assets/page_three/Item3/produto.png'),
//     type: 'Sale',
//     title: 'Camisa de seda parka mi…',
//     price: 'R$ 329',
//     oldPrice: 'R$ 698',
//     parcelado: '5x de R$ 105,80',
//   },
//   {
//     img: require('../../../../../assets/page_three/Item2/produto.png'),
//     type: 'preview | +2 cores',
//     title: 'Camisa de seda parka mi…',
//     price: 'R$ 329',
//     oldPrice: '',
//     parcelado: '5x de R$ 105,80',
//   },
// ];

interface Props {
  data: Array<any>;
}

const ListagemProdutos: React.FC<Props> = ({data}) => {
  const {singleItem} = useMenuSearch();
  console.log(data[0]);
  return (
    <Container>
      <Header />
      <FlatList
        contentContainerStyle={{alignItems: 'center'}}
        data={data}
        numColumns={singleItem ? 1 : 2}
        keyExtractor={(item, index) => String(index)}
        renderItem={({item, index}) => <Item index={index} {...item} />}
        style={{marginBottom: 20}}
      />
    </Container>
  );
};

export default ListagemProdutos;
