import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

interface Props {
  needMargin?: boolean;
}

export const Container = styled.TouchableOpacity<Props>`
  margin-top: 20px;
  width: ${width * 0.45}px;
  margin-left: ${(props) => (props.needMargin ? 5 : 0)}px;
`;

export const Image = styled.Image.attrs({
  resizeMode: 'stretch',
})`
  height: 281px;
  width: ${width * 0.45}px;
`;

export const ContainerInfo = styled.View`
  padding: 0 10px;
`;

export const Type = styled.Text`
  font-family: 'Futura Md BT';
  font-weight: bold;
  font-size: ${height * 0.018}px;
`;

export const Title = styled.Text`
  font-family: 'Futura Bk BT';
  font-size: ${height * 0.017}px;
`;

export const Price = styled.Text`
  color: red;
  font-weight: bold;
  font-size: ${height * 0.018}px;
`;

export const OldPrice = styled.Text`
  color: #969696;
  text-decoration-line: line-through;
  font-size: ${height * 0.018}px;
`;

export const Info = styled.Text`
  font-family: 'Futura Bk BT';
  font-size: ${height * 0.018}px;
`;
