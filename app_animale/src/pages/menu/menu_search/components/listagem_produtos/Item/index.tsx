import React from 'react';

import {
  Container,
  Image,
  Price,
  Title,
  Info,
  Type,
  ContainerInfo,
} from './styles';
import useMenuSearch from '../../../useMenuSearch';
import {useMemo} from 'react';

interface Props {
  image: string;
  type: string;
  name: string;
  price: string;
  oldPrice: string;
  index: number;
  parcelado: string;
}

const Item: React.FC<Props> = ({type, name, parcelado, image, index}) => {
  const {changeToDetails} = useMenuSearch();

  const needMargin = useMemo(() => index != 0 && index % 2 == 1, [index]);

  return (
    <Container needMargin={needMargin} onPress={changeToDetails}>
      <Image source={{uri: image}} />
      <ContainerInfo>
        <Type>{type}</Type>
        <Title>{name}</Title>
        <Price>R$ 529</Price>
        <Info>{parcelado}</Info>
      </ContainerInfo>
    </Container>
  );
};

export default Item;
