import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

const {width} = Dimensions.get('window');

export const Container = styled.View`
  flex-direction: row;
  width: 90%;
  align-self: center;
  justify-content: space-between;
  margin-top: 40px;
`;

export const Input = styled.TextInput.attrs({
  placeholder: 'Filtrar...',
})`
  width: 50%;
  padding-left: 15px;
  background-color: #f3f3f3;
  font-family: 'Futura Bk BT';
`;

export const Selector = styled(SectionedMultiSelect).attrs({
  uniqueKey: 'id',
  single: true,
  showChips: false,
  hideSearch: true,
  hideConfirm: true,
  alwaysShowSelectText: true,
  showCancelButton: false,
  styles: {
    selectToggle: {
      width: width * 0.4,
      backgroundColor: '#f3f3f3',
      padding: 15,
    },
    container: {
      padding: 20,
      flex: 0,
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    modalWrapper: {
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
})``;
