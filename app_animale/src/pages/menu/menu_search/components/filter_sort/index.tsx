import React from 'react';

import {Container, Selector, Input} from './styles';

interface Props {
  onChangeText: (text: string) => void;
}

const ordenar = [
  {
    name: 'Menor preço',
    id: 10,
  },
  {
    name: 'Maior preço',
    id: 17,
  },
  {
    name: 'Ordem alfabética A-Z',
    id: 13,
  },
  {
    name: 'Ordem alfabética Z-A',
    id: 14,
  },
];

const FilterSort: React.FC<Props> = ({onChangeText}) => {
  return (
    <Container>
      <Input onChangeText={onChangeText} />
      <Selector
        onSelectedItemsChange={() => {}}
        items={ordenar}
        selectText="Ordenar"
      />
    </Container>
  );
};

export default FilterSort;
