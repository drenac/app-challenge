import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';

import Home from '../pages/home';
import Menu from '../pages/menu';
import DetalheProduto from '../pages/detalhe_produto';
import Sacola from '../pages/sacola';

import Tabs from '../components/tabs';

import {ItenProvider} from '../context/useItems';

const BottomTabs = createBottomTabNavigator();

const Routes = () => {
  return (
    <ItenProvider>
      <NavigationContainer>
        <BottomTabs.Navigator tabBar={Tabs}>
          <BottomTabs.Screen component={Home} name="Home" />
          <BottomTabs.Screen
            options={{unmountOnBlur: true}}
            component={Menu}
            name="Menu"
          />
          <BottomTabs.Screen component={DetalheProduto} name="DetalheProduto" />
          <BottomTabs.Screen component={Sacola} name="Sacola" />
        </BottomTabs.Navigator>
      </NavigationContainer>
    </ItenProvider>
  );
};

export default Routes;
