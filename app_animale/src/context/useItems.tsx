import React, {useContext, useState, createContext} from 'react';

interface Props {
  itens: Array<any>;
  changeItens: (itens: Array<any>) => void;
}

const ItensContext = createContext({} as Props);

export const ItenProvider: React.FC = ({children}) => {
  const [itens, setItens] = useState<Array<any>>([]);

  function changeItens(newItens: Array<any>) {
    let array = new Array();
    newItens.forEach((item) => {
      array.push({
        name: item.productName,
        image: item.items[0].images[0].imageUrl,
      });
    });
    setItens(array);
  }

  return (
    <ItensContext.Provider value={{changeItens, itens}}>
      {children}
    </ItensContext.Provider>
  );
};

export default function () {
  return useContext(ItensContext);
}
